import React, {Component} from "react";
import GraphiQL from "graphiql";
import GraphiQLExplorer from "graphiql-explorer";
import {buildClientSchema, getIntrospectionQuery} from "graphql";

import "graphiql/graphiql.css";
import "./App.css";

// Collect the URL parameters
let parameters = {};

window.location.search.substr(1).split('&').forEach(function (entry) {
    let eq = entry.indexOf('=');
    if (eq >= 0) {
        parameters[decodeURIComponent(entry.slice(0, eq))] =
            decodeURIComponent(entry.slice(eq + 1));
    }
});

// Produce a Location query string from a parameter object.
function locationQuery(params) {
    return '?' + Object.keys(params).map(function (key) {
        return encodeURIComponent(key) + '=' +
            encodeURIComponent(params[key]);
    }).join('&');
}

// Derive a fetch URL from the current URL, sans the GraphQL parameters.
let graphqlParamNames = {
    query: true,
    variables: true,
    operationName: true
};

let otherParams = {};
for (let k in parameters) {
    if (parameters.hasOwnProperty(k) && graphqlParamNames[k] !== true) {
        otherParams[k] = parameters[k];
    }
}
let fetchURL = locationQuery(otherParams);


function graphQLFetcher(graphQLParams) {
    return fetch(fetchURL, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(graphQLParams),
        credentials: 'include',
    }).then(function (response) {
        return response.text();
    }).then(function (responseBody) {
        try {
            return JSON.parse(responseBody);
        } catch (error) {
            return responseBody;
        }
    });
}

let default_query = '';

if (window.hasOwnProperty('dq')) {
    default_query = window.dq;
}

let default_variables = '';

if (window.hasOwnProperty('dv')) {
    default_variables = window.dv;
}

class App extends Component {
    graphiql = null;
    state = {
        schema: null,
        query: default_query,
        variables: default_variables,
        explorerIsOpen: true
    };

    componentDidMount() {
        graphQLFetcher({
            query: getIntrospectionQuery()
        }).then(result => {
            if(result.hasOwnProperty('data')) {
                this.setState({schema: buildClientSchema(result.data)});
            }
        });
    }

    handleEditQuery = (query) => this.setState({query});
    handleEditVariables = (variables) => this.setState({variables});

    handleToggleExplorer = () => {
        this.setState({explorerIsOpen: !this.state.explorerIsOpen});
    };

    render() {
        const {query, variables, schema} = this.state;
        return (
            <div className="graphiql-container">
                <GraphiQLExplorer
                    schema={schema}
                    query={query}
                    onEdit={this.handleEditQuery}
                    onRunOperation={operationName =>
                        this.graphiql.handleRunQuery(operationName)
                    }
                    explorerIsOpen={this.state.explorerIsOpen}
                    onToggleExplorer={this.handleToggleExplorer}
                />
                <GraphiQL
                    ref={ref => (this.graphiql = ref)}
                    fetcher={graphQLFetcher}
                    schema={schema}
                    query={query}
                    variables={variables}
                    onEditQuery={this.handleEditQuery}
                    onEditVariables={this.handleEditVariables}
                >
                    <GraphiQL.Toolbar>
                        <GraphiQL.Button
                            onClick={() => this.graphiql.handlePrettifyQuery()}
                            label="Prettify"
                            title="Prettify Query (Shift-Ctrl-P)"
                        />
                        <GraphiQL.Button
                            onClick={() => this.graphiql.handleToggleHistory()}
                            label="History"
                            title="Show History"
                        />
                        <GraphiQL.Button
                            onClick={this.handleToggleExplorer}
                            label="Explorer"
                            title="Toggle Explorer"
                        />
                    </GraphiQL.Toolbar>
                </GraphiQL>
            </div>
        );
    }
}

export default App;
