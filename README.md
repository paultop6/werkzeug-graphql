# Werkzeug GraphQL

Minimal adapter to respond to Werkzeug web requests with a GraphQL schema (also can build a GraphQL schema from an ObjectQL type).